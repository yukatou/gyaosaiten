# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

names = %w/開発本部１ 営業本部１ 開発本部２ 営業本部２ コンテンツ２/

names.each do |name|
  Team.create!({ name: name })
end

#Vote.create!({ uid: "testuid1", team_id: 1, value: 10, profit: 10 })
#Vote.create!({ uid: "testuid2", team_id: 1, value: 1, profit: 2 })
#Vote.create!({ uid: "testuid1", team_id: 2, value: 3, profit: 8 })
