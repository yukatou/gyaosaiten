class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :uid
      t.integer :value
      t.integer :profit
      t.references :team, index: true

      t.timestamps null: false
    end
    add_foreign_key :votes, :teams
  end
end
