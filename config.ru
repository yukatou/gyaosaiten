# This file is used by Rack-based servers to start the application.
require 'grape/rabl'

Grape::Rabl.configure do |config|
  config.cache_template_loading = false # default: false
end

require ::File.expand_path('../config/environment', __FILE__)
run Rails.application
