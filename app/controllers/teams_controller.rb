class TeamsController < ApplicationController
  def index
    @teams = Team.all
  end

  def show
    @teams = Team.all
    @team = Team.find(params[:id])
    @votes = Vote.where(team_id: params[:id])
    
    @profit = 0
    @value = 0
    @votes.each do |vote|
        @profit += vote.profit
        @value += vote.value
    end
  end
end
