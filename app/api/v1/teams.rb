module V1
  class Teams < Grape::API
    helpers do
      # Strong Parametersの設定
      def message_board_params
        ActionController::Parameters.new(params).permit(:title, :body)
      end

      def set_message_board
        #@message_board = MessageBoard.find(params[:id])
      end

      # パラメータのチェック
      # パラメーターの必須、任意を指定することができる。
      # use :attributesという形で使うことができる。
      params :attributes do
        #requires :title, type: String, desc: "MessageBoard title."
        #optional :body, type: String, desc: "MessageBoard body."
      end

      # パラメータのチェック
      params :id do
        #requires :id, type: Integer, desc: "MessageBoard id."
      end
    end

    resource :teams do
      desc 'GET /api/v1/teams'
      get '/', rabl: 'teams.rabl' do
        @teams = Team.all
      end

      desc 'GET /api/v1/teams/:id'
      params do
        use :id
      end
      get '/:id', rabl: 'team.rabl' do
        @value = 0
        @profit = 0
        votes = Vote.where(team_id: params[:id])
        votes.each do |vote|
          @value += vote.value
          @profit += vote.profit
        end
      end
    end


  end
end
