module V1
  class Votes < Grape::API
    helpers do
      # Strong Parametersの設定
      def vote_params
        ActionController::Parameters.new(params).permit(:team_id, :uid, :profit, :value)
      end

      def set_vote
        #@message_board = MessageBoard.find(params[:id])
      end

      # パラメータのチェック
      # パラメーターの必須、任意を指定することができる。
      # use :attributesという形で使うことができる。
      params :attributes do
        requires :team_id, type: Integer, desc: "Vote Team ID."
        requires :uid, type: String, desc: "Vote UID."
        requires :profit, type: Integer, desc: "Vote Profit."
        requires :value, type: Integer, desc: "Vote Value."
        #optional :body, type: String, desc: "MessageBoard body."
      end
    end

    resource :votes do
      desc 'GET /api/v1/votes'
      get '/', rabl: 'votes.rabl' do
        @votes = Vote.all
      end

      desc 'POST /api/v1/votes'
      params do
        use :attributes
      end
      post '/' do
        vote = Vote.where(uid: params[:uid], team_id: params[:team_id]).first
        unless vote
            Vote.create!(vote_params)
        else
            vote.profit = params[:profit]
            vote.value = params[:value]
            vote.save!
        end
      end
    end
  end
end
