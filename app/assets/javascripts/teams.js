$(function() {

    $("#value-button").click(function() {
        $("#value-button").text($("#v").val());
    });

    $("#profit-button").click(function() {
        $("#profit-button").text($("#p").val());
    });

    $("#total-button").click(function() {
        $("#total-button").text(parseInt($("#p").val()) + parseInt($("#v").val()));
    });
});
